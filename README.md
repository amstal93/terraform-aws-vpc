# terraform-aws-vpc

## Prerequisites

- [Terraform](https://releases.hashicorp.com/terraform/). This module currently tested on `0.12.26`

## Quick Start

Terraform module to create VPC with two subnets type (public, private)

### Multi-Tier VPC

```hcl
module "abc_dev" {
  source  = "git::https://gitlab.com/rizkidoank/terraform-aws-vpc.git"
  version = "v0.1.0"
  
  environment    = "test"

  vpc_name       = "example-dev"
  vpc_description = "example dev vpc"

  vpc_cidr_block = "172.16.0.0/24"
  cidr_added_bits = 1

  subnet_availability_zones = ["ap-southeast-1a"]
}
```

### Module

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| cidr\_added\_bits | Added bits for the CIDR block for subnet calculation | `number` | n/a | yes |
| environment | Environment where the VPC provisioned | `string` | n/a | yes |
| subnet\_availability\_zones | List of subnets availability zones | `list` | n/a | yes |
| vpc\_cidr\_block | CIDR block reserved for the VPC | `string` | n/a | yes |
| vpc\_description | Description of the VPC | `string` | n/a | yes |
| vpc\_enable\_dns\_hostnames | Enable DNS hostnames for the VPC | `bool` | `true` | no |
| vpc\_enable\_dns\_support | Enable DNS support for the VPC | `bool` | `true` | no |
| vpc\_name | Name of the VPC | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| eip\_nat\_ids | List of Elastic IP allocation IDs for NAT Gateway. |
| eip\_nat\_public\_ips | List of Elastic IP  public IPs for NAT Gateway. |
| igw\_id | The ID of the Internet Gateway. |
| nat\_ids | List of NAT Gateway IDs |
| nat\_private\_ips | List of private IP addresses of the NAT Gateway. |
| rtb\_private\_ids | List of IDs of app route tables |
| rtb\_public\_id | ID of public route table |
| subnet\_private\_cidr\_blocks | List of cidr\_blocks of private subnets. |
| subnet\_private\_ids | List of IDs of private subnets. |
| subnet\_public\_cidr\_blocks | List of cidr\_blocks of public subnets. |
| subnet\_public\_ids | List of IDs of public subnets. |
| vpc\_cidr\_block | The CIDR block of the VPC. |
| vpc\_enable\_dns\_hostnames | Whether or not the VPC has DNS hostname support. |
| vpc\_enable\_dns\_support | Whether or not the VPC has DNS support. |
| vpc\_id | The ID of the VPC. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
